let menuBtn = document.getElementsByClassName('nav-btn')[0];
menuBtn.onclick = () => {
    let menuIcon = document.getElementById('menu-icon');
    menuIcon.classList.toggle('fa-times');
    let navMenu = document.getElementsByClassName('navbar-nav')[0];
    navMenu.classList.toggle('active');
};