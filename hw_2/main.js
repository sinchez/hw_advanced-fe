// Hamburger constructor
  function Hamburger(size, stuffing) {
    try {

      if (!arguments || arguments.length > 2) throw new HamburgerException('Не коректные данные');
      if (!size)  throw new HamburgerException('не передан размер');
      if (!stuffing)  throw new HamburgerException('не передан stuffing');
      this.size = size;
      this.stuffing = stuffing;
      this.toppings = [];
  
    } catch(error) {
      console.log(error);
    }
  }
  
  // Hamburger parameters
  Hamburger.SIZE_SMALL = {size: 'small', price: 50, calories: 20};
  Hamburger.SIZE_LARGE = {size: 'large', price: 100, calories: 40};
  Hamburger.STUFFING_CHEESE = {stuffing: 'cheese', price: 10, calories: 20};
  Hamburger.STUFFING_SALAD = {stuffing: 'salad', price: 20, calories: 5};
  Hamburger.STUFFING_POTATO = {stuffing: 'potato', price: 15, calories: 10};
  Hamburger.TOPPING_MAYO = {topping: 'mayo', price: 20, calories: 5};
  Hamburger.TOPPING_SPICE = {topping: 'spice', price: 15, calories: 0};
  Hamburger.TOPPING_S = {topping: '', price: 0, calories: 0};
  
  // Hamburger methods
  Hamburger.prototype.addTopping = function() {
    try {
      if (arguments.length === 0)   throw new HamburgerException('No toppings added');  
      for (var i = 0; i < arguments.length; i++)
        this.toppings.push(arguments[i]);
  
    } catch(error) {
      console.log(error);
    }
  };
  
  Hamburger.prototype.removeTopping = function(topping) {
    try {
      if (!this.toppings.includes(topping))
        throw new HamburgerException('Hamburger object does not have ' + topping.topping + ' topping to remove');
  
      this.toppings.splice(this.toppings.indexOf(topping), 1);
  
    } catch (error) {
      console.log(error);
    }
  }
  
  Hamburger.prototype.getToppings = function() {
    return this.toppings;
  }
  
  Hamburger.prototype.getSize = function() {
    return this.size.size;
  }
  
  Hamburger.prototype.getStuffing = function() {
    return this.stuffing.stuffing;
  }
  
  Hamburger.prototype.calculatePrice = function() {
    var sizePrice = this.size.price;
    var stuffingPrice = this.stuffing.price;
    var toppingsPrice = this.toppings.reduce(function(sum, topping) {
      return sum + topping.price;
    }, 0);
  
    return sizePrice + stuffingPrice + toppingsPrice;
  }
  
  Hamburger.prototype.calculateCalories = function() {
    var toppingsCalories = this.toppings.reduce(function(sum, topping) {
      return sum + topping.calories;
    }, 0);
  
    return toppingsCalories + this.size.calories + this.stuffing.calories;
  }
  
  // Hamburger exception
  function HamburgerException(message) {
    this.name = 'Hamburger Exception';
    this.message = message;
  }
  HamburgerException.prototype = Error.prototype;
  
  // create hamburger
  var h = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
  var f = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_CHEESE);
  // test hamburger
  h.addTopping(Hamburger.TOPPING_MAYO, Hamburger.TOPPING_SPICE);
  f.addTopping(Hamburger.TOPPING_MAYO, Hamburger.TOPPING_S);
  console.log('toppings:', h.getToppings());
  console.log('size: ' + h.getSize());
  console.log('stuffing: ' + h.getStuffing());
  console.log('price: ' + h.calculatePrice() + ' UAH');
  console.log('calories: ' + h.calculateCalories());
  

  console.log('toppings:', f.getToppings());
  console.log('size: ' + f.getSize());
  console.log('stuffing: ' + f.getStuffing());
  console.log('price: ' + f.calculatePrice() + ' UAH');
  console.log('calories: ' + f.calculateCalories());
//    h.removeTopping(Hamburger.TOPPING_SPICE);
//   console.log('remaining toppings:', h.getToppings());
//   console.log('remaining toppings:', f.getToppings());